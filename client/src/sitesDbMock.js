export const sitesDb = [
  {
    titleText: 'reactjs.org',
    url: 'reactjs.org',
    description: 'React · Declarative. React makes it painless to create interactive UIs.',
  },
  {
    titleText: 'React (JavaScript library) - Wikipedia',
    url: 'https://en.wikipedia.org/wiki/React_(JavaScript_library)',
    description: 'React (also known as React.js or ReactJS) is a free and open-source front-end JavaScript library for building user interfaces based on UI components.',
  },
  {
    titleText: 'React is a JavaScript library for building user interfaces. - GitHub',
    url: 'https://github.com/facebook/react',
    description: 'React is a JavaScript library for building user interfaces. Declarative: React makes it painless to create interactive UIs.',
  },
  {
    titleText: 'React Native · Learn once, write anywhere',
    url: 'https://reactnative.dev/',
    description: 'React Native combines the best parts of native development with React, a best-in-class JavaScript library for building user interfaces.',
  },
  {
    titleText: 'What is React - W3Schools',
    url: 'https://www.w3schools.com/whatis/whatis_react.asp',
    description: 'React applications are usually built around a single HTML element.',
  },
  {
    titleText: 'React-Bootstrap · React-Bootstrap Documentation',
    url: 'https://react-bootstrap.github.io/',
    description: 'The most popular front-end framework, rebuilt for React.',
  },
  {
    titleText: 'Getting started with React - Learn web development | MDN',
    url: 'https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/React_getting_started',
    description: 'As its official tagline states, React is a library for building user interfaces. React is not a framework – it\'s not even exclusive to the web',
  },
  {
    titleText: 'The Complete Course (incl. React Router 4 & Redux) - Udemy',
    url: 'https://www.udemy.com/course/react-the-complete-guide-incl-redux/',
    description: 'Dive in and learn React.js from scratch! Learn Reactjs, Hooks, Redux, React Routing, Animations, Next.js and way more!',
  },
  {
    titleText: 'National and Local Weather Radar, Daily Forecast',
    url: 'https://weather.com/',
    description: 'The Weather Channel and weather.com provide a national and local weather forecast for cities, as well as weather radar, report and hurricane coverage.'
  },
  {
    titleText: 'Weather forecasts for thousands of locations around the world',
    url: 'https://www.bbc.com/weather',
    description: 'Latest weather conditions and forecasts for the UK and the world. Includes up to 14-days of hourly forecast information, warnings, maps.'
  },
  {
    titleText: '1Weather Forecasts & Radar',
    url: 'https://play.google.com/store/apps/details?id=com.handmark.expressweather&hl=en&gl=US&pli=1',
    description: 'Hyperlocal weather forecasts to make your life easier. #1 Weather app, trusted by 50 Million+ Android Users. Ranked as the best app during the 2021 Atlantic Hurricane Season.'
  },
  {
    titleText: '8 Best Weather Websites For Accurate Forecast In 2022',
    url: 'https://fossbytes.com/best-weather-websites/',
    description: 'Do you suffer from sudden weather change blues? Access these weather websites and have a better plan for such days'
  },
  {
    titleText: 'World Weather Information Service',
    url: 'https://worldweather.wmo.int/en/home.html',
    description: 'This global website presents OFFICIAL weather observations, weather forecasts and climatological information for selected cities supplied by National Meteorological & Hydrological Services (NMHSs) worldwide.'
  },
  {
    titleText: 'Sailing Weather - Marine Weather Forecasts for Sailors',
    url: 'https://www.passageweather.com/',
    description: 'The WW3 model is also run four times daily, with forecast output to 180 hours (7.5 days).'
  },
  {
    titleText: 'Why Choose PredictWind Forecasts',
    url: 'https://www.predictwind.com/why-predictwind-forecast/',
    description: 'PredictWind’s goal is to deliver the most accurate forecast data available. Having access to the world’s top forecast models enables users to be assured of the best possible forecast. With the top-ranked global models, along with the high-resolution PredictWind modeling and the best regional models, PredictWind offers unparalleled forecast accuracy.'
  },
  {
    titleText: '42 (2013) - IMDb',
    url: 'https://www.imdb.com/title/tt0453562/',
    description: 'The life story of Jackie Robinson and his history-making signing with the Brooklyn Dodgers.'
  },
  {
    titleText: 'For Math Fans: A Hitchhiker’s Guide to the Number 42',
    url: 'https://www.scientificamerican.com/article/for-math-fans-a-hitchhikers-guide-to-the-number-42/',
    description: 'To find out what happens next, you`ll have to read Adams`s books. The author`s choice of the number 42 has become a fixture of geek culture.'
  },
  {
    titleText: 'DEFINITION 42 (h2g2, meaning of life, The Hitchhikers Guide to the Galaxy)',
    url: 'https://www.techtarget.com/whatis/definition/42-h2g2-meaning-of-life-The-Hitchhikers-Guide-to-the-Galaxy',
    description: 'Forty-two is the ASCII code for the symbol * also known as the asterisk. This symbol is often thought to translate to anything or everything.'
  },

  {
    titleText: 'Area Gamma Detector GM-42 - Rotem Radiation',
    url: 'https://www.rotem-radiation.co.il/productmed/area-gamma-detector-gm-42/',
    description: 'The GM-42 Detector is a sensitive low range gamma radiation detector used to measure dose rates in areas where radioactive material'
  },
  {
    titleText: 'Rolex Yacht-Master 42 watch: 18 ct yellow gold - M226658-0001',
    url: 'https://www.rolex.com/watches/yacht-master/m226658-0001.html',
    description: 'The Oyster Perpetual Yacht-Master 42 in 18 ct yellow gold with a black dial and an Oysterflex bracelet.'
  },
  {
    titleText: 'The behaviour change wheel: A new method for characterising and designing behaviour change interventions',
    url: 'https://implementationscience.biomedcentral.com/articles/10.1186/1748-5908-6-42',
    description: 'Implementation Science volume 6, Article number: 42 (2011) Cite this article. 648k Accesses. 4377 Citations. 1104 Altmetric.'
  },
  {
    titleText: 'Ethereum USD (ETH-USD) Price, Value, News & History',
    url: 'https://finance.yahoo.com/quote/ETH-USD/?guccounter=1&guce_referrer=aHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS8&guce_referrer_sig=AQAAAKKC_AOHyXW7v47HIkUf66EaoXqF4SXe_9Pp9uIxu1-0KH7pUbIoz5AAeZvNJO6PiS5lTUrbgZb9Z4dGCxTm885tfp2i_tXtO7Oi3Z_1-UNrFfmLlWlUvwM725cuMrBDzlAllxBf_BCQkkuw6WiN8DHiV9E7xg7GqdFZmR3ywbtX',
    description: 'ETH-USD - Ethereum USD ; Open, 1,167.58 ; Day`s Range, 1,165.41 - 1,184.68 ; 52 Week Range, 896.11 - 4,149.03 ; Start Date, 2015-08-07'
  },
  {
    titleText: 'USD ETH | US Dollar Ethereum - Investing.com',
    url: 'https://www.investing.com/currencies/usd-eth',
    description: 'Find the current US Dollar Ethereum rate and access to our USD ETH converter, charts, historical data, news, and more.'
  },
  {
    titleText: 'Ethereum price today, ETH to USD live, marketcap and chart',
    url: 'https://coinmarketcap.com/currencies/ethereum/',
    description: 'The live Ethereum price today is $1,178.39 USD with a 24-hour trading volume of $7,032,631,504 USD. We update our ETH to USD price in real-time.'
  },
  {
    titleText: 'Ethereum USD (ETH-USD) Stock Price Today, Quote & News',
    url: 'https://seekingalpha.com/symbol/ETH-USD',
    description: ' Get the latest Ethereum USD (ETH-USD) cryptocurrency price, charts, news, analysis, and investment tools.'
  },
  {
    titleText: 'ETH to USD | Ethereum Price Index & Live Price Chart - CEX.IO',
    url: 'https://cex.io/eth-usd',
    description: 'Newbie or guru, everyone needs a live chart to know the Ethereum price. The most popular quote currency is the US dollar so enjoy watching the ETH to USD'
  },
  {
    titleText: 'Ethereum Price Chart (ETH/USD) - CoinGecko',
    url: 'https://www.coingecko.com/en/coins/ethereum',
    description: 'Updated 17 December 2022: Current price of Ethereum is USD $1174.74 with a 24-hour trading volume of $6599720724'
  },
  {
    titleText: 'US-Dollar-Ethereum | USD/ETH | aktueller Wechselkurs',
    url: 'https://www.finanzen.net/devisen/us_dollar-ethereum-kurs',
    description: 'USD/ETH: Aktueller US-Dollar - Ethereum Kurs heute mit Chart, historischen Kursen und Nachrichten. Wechselkurs USD in ETH.'
  },
  {
    titleText: 'ETH Price Index, Live Chart and USD Converter - Binance',
    url: 'https://www.binance.com/en/price/ethereum',
    description: '24-hour trading volume is $8.14B USD. ETH to USD price is updated in real-time. Ethereum is -5.42% in the last 24 hours.'
  }
];
