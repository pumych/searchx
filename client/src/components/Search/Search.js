import React, { useState, useRef, useEffect, useMemo } from 'react';
import styled from 'styled-components';
import OutsideClickHandler from 'react-outside-click-handler';
import { config } from '../../consfig';


const Search = ({ handleSelectedValue, sitesDb }) => {
  const inputElement = useRef(null);
  const [ searchInput, setSearchInput ] = useState('');
  const [ isAutocompleteVisible, setIsAutocompleteVisible ] = useState(false);
  const [ searchHistory, setSearchHistory ] = useState(config.searchHistoryInit);

  useEffect(() => {
    if (inputElement.current) {
      inputElement.current.focus();
    }
  }, []);

  const autocompleteListItems = useMemo(() => getAutocompleteList({dbResults: sitesDb, historyResults: searchHistory, inputValue: searchInput, limitSize: config.maxHistoryItemsVisible}), [sitesDb, searchHistory, searchInput]);

  return (
    <StyledSearch>
      <StyledSearchInput
             type="text"
             ref={inputElement}
             onChange={(e) => handleInputChange(e.target.value)}
             onClick={() => setIsAutocompleteVisible(true)}
             onKeyDown={handleKeyDown}
             value={searchInput}
      />
      <StyledSearchButton type="button" value="Search" onClick={() => handleSelect()} >🔍</StyledSearchButton>
      { isAutocompleteVisible
        && <OutsideClickHandler onOutsideClick={() => setIsAutocompleteVisible(false)}>
          <StyledAutocompleteList
            listItems={autocompleteListItems}
            inputValue={searchInput}
            handleDropdownSelect={handleDropdownSelect}
            removeElementFromHistory={removeElementFromHistory}
          />
      </OutsideClickHandler> }
    </StyledSearch>
  );

  function handleInputChange(text) {
    setSearchInput(text);
    setIsAutocompleteVisible(true);
  }

  function handleSelect() {
    searchInput && setSearchHistory([searchInput, ...searchHistory.filter( str => str !== searchInput )]);
    handleSelectedValue(searchInput);
    setIsAutocompleteVisible(false);
  }

  function handleDropdownSelect(clickedVal) {
    setSearchInput(clickedVal);
    handleSelectedValue(clickedVal);
    setIsAutocompleteVisible(false);
  }

  function handleKeyDown(e) {
    if(e.key === 'Enter') {
      handleSelect();
    }
  }

  function removeElementFromHistory(val) {
    setSearchHistory(searchHistory.filter( historyItem => historyItem !== val ));
  }
};

const getAutocompleteList = ({ dbResults, historyResults, inputValue, limitSize }) => {
  const autocompleteList = [];
  for(let i = 0; i < historyResults.length; i++) {
    autocompleteList.push({ label: historyResults[i], isSearchHistory: true });
  }

  if(inputValue){
    for(let i = 0; i < dbResults.length; i++ ) {
      /** Note: here, in real solution we will use whole result title lookup to match the text, not only the 'startWith' */
      if(dbResults[i].titleText.toLowerCase().startsWith(inputValue)) {
        autocompleteList.push({ label: dbResults[i].titleText, isSearchHistory: false });
      }
    }
  }

  return autocompleteList.slice(0, limitSize);
};

const AutocompleteList = ({ listItems, inputValue, handleDropdownSelect, className, removeElementFromHistory }) => {
  const autocompleteItems = listItems.filter(onlyUnique).filter( resultItem => resultItem.label.startsWith(inputValue) )
    .map( (resultItem, index) => <AutocompleteItem key={`${resultItem.label}+${index}`} resultItem={resultItem} handleDropdownSelect={handleDropdownSelect} removeElementFromHistory={removeElementFromHistory} /> );
  return(
    <div>{autocompleteItems.length > 0 ? <ul className={className}>{autocompleteItems}</ul> : <ul className={className}><li style={{ padding: '0.5rem 1rem' }}>-- no results --</li></ul>}</div>
  );
};

const AutocompleteItem = ({ resultItem, handleDropdownSelect, removeElementFromHistory }) => {
  return <StyledSearchResultItem
    style={{ cursor: 'pointer' }}
    className={resultItem.isSearchHistory ? 'is-search-history' : '' }
  >
    <span className="label" onClick={() => handleDropdownSelect(resultItem.label)}>{resultItem.label}</span>
    {resultItem.isSearchHistory && <span className="delete" onClick={() => removeElementFromHistory(resultItem.label)}>Delete</span>}
  </StyledSearchResultItem>;
};

const StyledSearchResultItem = styled.li`
  cursor: pointer;
  position: relative;
  
  &.is-search-history {
    color: #52188c;
  }
  
  .label, .delete {
    padding: 0.5rem 1rem;
    display: block;
  }
  
  .delete {
    visibility: hidden;
    position: absolute;
    right: 0rem;
    color: #ccc;
    font-size: 0.8rem;
    top: 0;
    padding-right: 1rem;
  }
  
  &:hover {
    .delete {
      visibility: visible; 
      
      &:hover {
        text-decoration: underline;
      }
    }
  }
`;

const onlyUnique = (value, index, self) => (self.indexOf(value) === index);

const StyledSearch = styled.div`
  margin-top: 2rem;
  position: relative;
  z-index: 3;
`;

const StyledSearchInput = styled.input`
  box-sizing: border-box;
  padding: 5px 12px;
  font-size: 14px;
  line-height: 20px;
  color: #24292e;
  vertical-align: middle;
  background-color: #ffffff;
  background-repeat: no-repeat;
  background-position: right 8px center;
  border: 1px solid #e1e4e8;
  border-radius: 6px;
  outline: none;
  box-shadow: rgba(225, 228, 232, 0.2) 0px 1px 0px 0px inset;
  :focus{
      border-color: #0366d6;
      outline: none;
      // box-shadow: rgba(3, 102, 214, 0.3) 0px 0px 0px 3px;
  }  
  margin-right: 1rem;
  width: 100%; 
`;

const StyledSearchButton = styled.span`
  display: inline-block;
  cursor: pointer;
  position:absolute;
  top: 0.2rem;
  right: 0.5rem;
`;

const StyledAutocompleteList = styled(AutocompleteList)`
  list-style-type: none;
  background: #fff;
  padding: 0;
  margin: 0;
  box-shadow: 0 6.4px 14.4px 0 rgb(0 0 0 / 13%), 0 1.2px 3.6px 0 rgb(0 0 0 / 11%);
  position: absolute;
  width: 650px;
`;

export default Search;
