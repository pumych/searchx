import React, { useState, useMemo } from 'react';
import styled from 'styled-components';
import Search from "./components/Search/Search";
import {sitesDb} from "./sitesDbMock";
import { config } from './consfig';


function App() {
  const [ selectedValue, setSelectedValue ] = useState('');
  const foundSites = useMemo(() => sitesDb.filter((site) => site.titleText.toLowerCase().includes(selectedValue) || site.description.toLowerCase().includes(selectedValue)), [selectedValue]);

  return (
    <StyledApp className="App">
      <h3 style={{ textAlign: 'center', marginTop: '2rem'}}>SEARCH X</h3>
      <Search handleSelectedValue={handleSelectedValue} sitesDb={sitesDb}  />
      {selectedValue && <StyledSearchResults searchResults={foundSites} />}
    </StyledApp>
  );

  function handleSelectedValue(selectedSearchValue) {
    setSelectedValue(selectedSearchValue);
  }
}

const SearchResults = ({ searchResults, className }) => {
  const [currentPage, setCurrentPage] = useState(1);

  return(
    <>
      {searchResults.length > 0 ?
        <ul className={className}>
          {searchResults.filter((result, index) => index > (currentPage-1) * config.ITEMS_PER_PAGE).splice(0, config.ITEMS_PER_PAGE).map((result, index) => (<StyledResult key={`${result.titleText}_${index}`}>
            <div className="title" style={{textDecoration: 'underline', color: 'blue', cursor: 'pointer'}}
                 onClick={() => window.open(result.url, '_blank')}>{result.titleText}</div>
            <div className="description">{result.description}</div>
          </StyledResult>))}
        </ul>:
        <div style={{paddingTop: '2rem', paddingLeft: '1rem'}}>Your search did not match any documents.</div>
      }
      <Pagination totalItems={searchResults.length} itemsPerPage={config.ITEMS_PER_PAGE} currentPage={currentPage} handleClick={(val) => setCurrentPage(val)} />
    </>
  );
};

const Pagination = ({ totalItems, itemsPerPage, currentPage, handleClick }) => {
  const totalPages = Math.ceil(totalItems/(itemsPerPage));

  return(<div style={{ textAlign: 'center' }}>
    {totalPages > 1 && <div style={{ display: 'inline-block' }}>
      { currentPage !== 1 && <StyledLink onClick={() => handleClick(currentPage-1)}>&lt;</StyledLink>}
      {[...Array(totalPages)].map( (x, index) => <StyledLink key={`page-link-${index}`} active={currentPage === (index + 1)} onClick={() => currentPage !== (index + 1) && handleClick(index+1)}>{index+1}</StyledLink> )}
      {currentPage !== totalPages && <StyledLink onClick={() => handleClick(currentPage+1)}>&gt;</StyledLink>}
    </div>}
  </div>);
};

const StyledLink = styled.span`
  ${(props) => props.active ? '' : 'color: blue;'}
  margin: 0 3px;
  ${(props) => props.active ? '' : 'cursor: pointer;'}
  &:hover {
    ${(props) => props.active ? '' : 'text-decoration: underline;'}
  }
`;

const StyledApp = styled.div`
  font-family: Arial, Helvetica, sans-serif;
  width: 650px;
  margin: 0 auto;
`;

const StyledSearchResults = styled(SearchResults)`
  padding: 0 0 0 1rem;
`;

const StyledResult = styled.li`
  list-style-type: none;
  padding: 1rem 0;
  margin: 0;
`;

export default App;


