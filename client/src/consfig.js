export const config = {
  searchHistoryInit: ['weather', '42', 'react', 'usd'],
  maxHistoryItemsVisible: 10,
  ITEMS_PER_PAGE: 5
};
